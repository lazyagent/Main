"""
The `utils` module contains utilities needed by other parts of the project like
a parser

In this module, for safity, just wrapper functions are imported from submodules
and developers may not to refer and use inside functions directly! (except for
debugging proposes)
"""
from .__parser import parse

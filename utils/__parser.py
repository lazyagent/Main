#! /usr/bin/env python
#-*- encoding: 'utf-8' -*-

## In The Name of Allah

import re

class Parsed(object):
    """
This class is just a placeholder for an object that be used after parsing.

Exactly, the process of parsing does a monkeypatch into an instance of this
class.
    """
    pass

def parse(inp, phase):
    """
This is the main parser function.

This function gives an argument `inp` as the raw input and argument `phase` as
indicator of the phase accorded to the raw input, then returns an object that
has the needed properties to create a world model for the agent.

The `phase` argument is interpreted as:
    0: The `inp` is a raw input for First Phase
    1: The `inp` is a raw input for First part of Second Phase
    2: The `inp` is a raw input for Second (final) part of Second Phase
    """
    if None in (inp, phase): raise TypeError('All argumets are mandatory')
    if inp[-1] != ']' or inp[0] != '[': raise TypeError('Malformed input')
    if phase == 0:
        return _parse_1(inp)
    elif phase == 1:
        return _parse_21(inp)
    elif phase == 2:
        return _parse_22(inp)
    else:
        raise TypeError('Bad phase')

def _parse_1(inp):
    inp = inp.replace('_', "[ ]")   ## The process of hacking the `_` in the input!!
    ll = re.findall('\[.+?\]', inp) ## The input is tokenized into its parts in a non-greedy manner (The `?` after `+` made non-greedy)
    parsed_object = Parsed()
    parsed_object.size = _extractSize(ll[0])
    parsed_object.tasks = _extractTasks(ll[1])
    parsed_object.taskQ = _extractTaskQ(ll[2])
    parsed_object.blocks = _extractBlocks(ll[3])
    parsed_object.initLoc = _extractLoc(ll[4])
    parsed_object.energy = _extractEnergy(inp)

    return parsed_object

def _parse_21(inp):
    inp = inp.replace('_', "[ ]")   ## Same as above
    ll = re.findall('\[.+?\]', inp) ## Same as above
    parsed_object = Parsed()
    parsed_object.size = _extractSize(ll[0])
    parsed_object.tasks = _extractTasks(ll[1])
    parsed_object.taskQ = _extractTaskQ(ll[2])
    parsed_object.blocks = _extractBlocks(ll[3])
    parsed_object.hole = _extractHoles(ll[4])
    parsed_object.initLoc = _extractLoc(ll[5])
    parsed_object.energy = _extractEnergy(inp)

    return parsed_object

def _parse_22(inp):
    inp = inp.replace('_', "[ ]")   ## Same as above
    ll = re.findall('\[.+?\]', inp) ## Same as above
    parsed_object = Parsed()
    parsed_object.size = _extractSize(ll[0])
    parsed_object.tasks = _extractTasks(ll[1])
    parsed_object.taskQ = _extractTaskQ(ll[2])
    parsed_object.blocks = _extractBlocks(ll[3])
    parsed_object.hole = _extractHoles(ll[4])
    parsed_object.battery = _extractBatteries(ll[5])
    parsed_object.initLoc = _extractLoc(ll[6])
    parsed_object.energy = _extractEnergy(inp)

    return parsed_object

def _extractSize(ss):
    x, y = re.findall('\[\s*(\d+)\s*,\s*(\d+)\s*\]', ss)[0] ## I'll assume the regex tells the story!
    return (int(x), int(y))

def _extractTasks(ss):
    return eval(ss) ## The format is exactly as a python list within tuples! So just evaluate it!

def _extractTaskQ(ss):
    return re.findall('\s*(\d+)\s*->\s*(\d+)\s*', ss)   ## Also here I'll assume the regex is enaugh talking!

def _extractBlocks(ss):
    return eval(ss) ## Same as above `eval` call

def _extractHoles(ss):
    return _extractTasks(ss)    ## Similarity in format made the work

def _extractBatteries(ss):
    return _extractTasks(ss)    ## Same as above

def _extractLoc(ss):
    x, y = re.findall('\[\s*(\d+)\s*,\s*(\d+)\s*\]', ss)[0] ## Also like other regexes ...! :D
    return (int(x), int(y))

def _extractEnergy(ss):
    ## This regex is the trikiest part! I'd used a non-capturing group (the
    ## `(?: ... )` syntax) for the decimal part as the decimal part may
    ## not be written! (like 25 or 14.5) and we need a way to capture both!
    return float(re.findall('.*,\s*(\d+(?:\.\d+)?)\]$', ss)[0])
